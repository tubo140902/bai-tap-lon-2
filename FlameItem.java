package bomberman.entities.tiles;

import bomberman.Game;
import bomberman.audio.Audio;
import bomberman.entities.Entity;
import bomberman.entities.characters.Bomber;
import bomberman.graphics.Sprite;

public class FlameItem extends Item {
    protected boolean _active;

    public FlameItem(int x, int y, Sprite sprite) {
        super(x, y, sprite);
        _active = false;
    }

    @Override
    public boolean collide(Entity e) {
        // TODO: xử lý Bomber ăn Item
        if(e instanceof Bomber) {
            Audio powerUpAudio = new Audio(Audio.POWER_UP);
            powerUpAudio.play();

            if (!_active) {
                _active = true;
                Game.addBombRadius(1);
            }
            remove();
            return true;
        }
        return false;
    }

}
