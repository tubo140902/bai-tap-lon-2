package bomberman.entities.tiles;

import bomberman.graphics.Sprite;

public class Wall extends Tile {

    public Wall(int x, int y, Sprite sprite) {
        super(x, y, sprite);
    }

}
