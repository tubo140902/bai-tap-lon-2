package bomberman.entities;

import bomberman.graphics.iRender;
import bomberman.graphics.Screen;
import bomberman.graphics.Sprite;
import bomberman.Level.Coordinates;
public abstract class Entity implements iRender {
    protected double _x, _y;
    protected boolean _removed = false;
    protected Sprite _sprite;

    @Override
    public abstract void update();
    @Override
    public abstract void render(Screen screen);

    public void remove() {
        _removed = true;
    }

    public boolean isRemoved() {
        return _removed;
    }

    public Sprite getSprite() {
        return _sprite;
    }

    /**
     * Phương thức này được gọi để xử lý khi hai entity va chạm vào nhau
     * @param e
     * @return
     */
    public abstract boolean collide(Entity e);

    public double getX() {
        return _x;
    }

    public double getY() {
        return _y;
    }

    public int getXTile() {
        return Coordinates.pixelToTile(_x + _sprite.SIZE / 2);
    }

    public int getYTile() {
        return Coordinates.pixelToTile(_y - _sprite.SIZE / 2);
    }
}
