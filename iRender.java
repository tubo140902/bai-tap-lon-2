package bomberman.graphics;

public interface iRender {
    void update();
    void render(Screen screen);
}
